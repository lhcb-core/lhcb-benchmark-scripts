#!/bin/bash

./cleanup.sh

$(dirname $0)/../run_throughput_jobs.py -n 2e6 '$MOOREROOT/options/force_functor_cache.py' '$HLT1CONFROOT/options/hlt1_pp_track_mva.py'

$(dirname $0)/../run_throughput_jobs.py -n=-1 -j 1 --profile '$MOOREROOT/options/force_functor_cache.py' '$HLT1CONFROOT/options/hlt1_pp_track_mva.py'
