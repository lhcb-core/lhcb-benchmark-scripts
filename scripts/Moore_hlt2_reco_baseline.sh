#!/bin/bash

./cleanup.sh

if [[ $(hostname --fqdn) == "lbhltperf01.cern.ch" ]]; then
  # this test runs on lbhltperf01 where we have more space for HLT2 input at
  cache_dirs=(--cache-dirs "/scratch/z5/data/Hlt2Throughput")
fi

$(dirname $0)/../run_throughput_jobs.py -n 2e4 --test-file-db-key=UpgradeHLT1FilteredWithGEC '$MOOREROOT/options/ft_decoding_v2.py' '$RECOCONFROOT/options/hlt2_reco_baseline.py' "${cache_dirs[@]}"

$(dirname $0)/../run_throughput_jobs.py -n=-1 -j 1 --profile --test-file-db-key=UpgradeHLT1FilteredWithGEC '$MOOREROOT/options/ft_decoding_v2.py' '$RECOCONFROOT/options/hlt2_reco_baseline.py' "${cache_dirs[@]}"
