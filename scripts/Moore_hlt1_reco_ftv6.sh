#!/bin/bash

./cleanup.sh

$(dirname $0)/../run_throughput_jobs.py -n 2e6 --test-file-db-key=MiniBrunel_2019_MinBias_FTv6_MDF '$MOOREROOT/options/ft_decoding_v6.py' '$RECOCONFROOT/options/hlt1_reco_baseline.py'

$(dirname $0)/../run_throughput_jobs.py -n=-1 -j 1 --test-file-db-key=MiniBrunel_2019_MinBias_FTv6_MDF --profile '$MOOREROOT/options/ft_decoding_v6.py' '$RECOCONFROOT/options/hlt1_reco_baseline.py'
