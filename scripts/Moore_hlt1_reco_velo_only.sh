#!/bin/bash

./cleanup.sh

$(dirname $0)/../run_throughput_jobs.py -n 3e6 '$RECOCONFROOT/options/hlt1_reco_velo_only.py'

$(dirname $0)/../run_throughput_jobs.py -n=-1 -j 1 --profile '$RECOCONFROOT/options/hlt1_reco_velo_only.py'
