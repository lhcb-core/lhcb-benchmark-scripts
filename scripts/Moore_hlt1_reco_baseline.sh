#!/bin/bash

./cleanup.sh

$(dirname $0)/../run_throughput_jobs.py -n 2e6 '$RECOCONFROOT/options/hlt1_reco_baseline.py'

$(dirname $0)/../run_throughput_jobs.py -n=-1 -j 1 --profile '$RECOCONFROOT/options/hlt1_reco_baseline.py'
