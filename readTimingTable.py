from __future__ import print_function, division, absolute_import
import pandas
import os, sys, re
from io import StringIO, open
from collections import OrderedDict
"""
Define the algorithm names that are grouped under different categories.
The algorithms are arranged as a list of (possible regular) expressions
This is an OrderedDict so that if an algorithm matches more than one group
it is added to the first one
"""
algGroups = {
    "HLT1":
    OrderedDict([
        ("Framework", ["LHCb__MDF__IOAlg", "reserveIOV", "DummyEventTime"]),
        ("VeloCluster",
         ["VeloClusterTrackingSIMD", "TrackBeamLineVertexFinderSoA"]),
        ("GEC", ["PrGECFilter"]),
        ("VeloUT", ["PrStorePrUTHits", "PrVeloUT"]),
        ("Forward", ["FTRawBankDecoder", "SciFiTrackForwarding.*"]),
        ("VeloKalman", ["VeloKalman"]),
        ("Muon", [".*Muon.*"]),
    ]),
    "HLT2":
    OrderedDict([
        ("Framework",
         ["LHCb__MDF__IOAlg", "reserveIOV", "createODIN", "DummyEventTime"]),
        (
            "HLT1",
            [
                "VeloClusterTrackingSIMD",
                "PrGECFilter",
                "TrackBeamLineVertexFinderSoA",
                "VPClus",
                "PrVeloUT",
                "PrStoreUTHit",
                "PrStorePrUTHits",
                "VeloKalman",
                "FTRawBankDecoder",
                "PrStoreFTHit",
                "SciFiTrackForwarding.*"  #contains both the algorithm and the hit-storing
            ]),
        ("Forward", ["PrStoreSciFiHits", "PrForwardTrackingVelo"]),
        ("Seeding", ["PrHybridSeeding"]),
        ("Match", ["PrMatchNN"]),
        ("Downstream", ["PrLongLivedTracking"]),
        ("TrackFit", ["TrackBestTrackCreator"]),
        ("Calorimeter", [
            "FutureEcalZSup", "FutureCellularAutomatonAlg",
            "CaloFutureShowerOverlap", "CaloFutureClusterCovarianceAlg",
            "CaloFutureMergedPi0", "InEcalFutureAcceptanceAlg",
            "FutureTrack2EcalEAlg", "FutureEcalPIDmuAlg", "FutureHcalZSup",
            "InHcalFutureAcceptanceAlg", "FutureTrack2HcalEAlg",
            "FutureHcalPIDeAlg", "FutureHcalPIDmuAlg", "FuturePhotonMatchAlg",
            "ClassifyPhotonElectronAlg", "FutureElectronMatchAlg",
            "FutureEcalChi22ID", "FutureEcalPIDeAlg",
            "FutureInBremFutureAcceptanceAlg", "BremMatchAlgFuture",
            "BremChi22IDFuture", "BremPIDeAlgFuture", "FutureClusChi22ID"
        ]),
        ("RICH", [
            ".*?[r,R][i,I][c,C][h,H].*"
        ]),  #matches any combination of uppercase-lowercase mention of RICH
        ("Converters",
         [".*?[c,C]onverter.*"]),  #matches either Converter or converter
    ])
}


def parseTimingTable(log_fn):
    TIMING_TABLE_RE = re.compile(
        r"\n"
        r"HLTControlFlowMgr +INFO Timing table:\n"
        r"HLTControlFlowMgr +INFO Average ticks per millisecond: (?P<ticks>\d+)\n"
        r"HLTControlFlowMgr +INFO +\n"
        r"(?P<table>(?: \|.*\|\n)+)")
    with open(log_fn, encoding='utf-8') as f:
        m = TIMING_TABLE_RE.search(f.read())
        if not m:
            raise RuntimeError("Timing table not found in {}".format(log_fn))
    table_str = m.group("table")
    df = pandas.read_csv(StringIO(table_str), sep=r"\s*\|\s*", engine="python")
    df = df.drop(columns=["Unnamed: 0", "Unnamed: 5"])
    df.columns = ["name", "count", "total_time", "average_time"]
    df.set_index("name")
    df.name = map(lambda x: x.strip('"'), df.name)
    return df


def parseAllTimingTables(listOfLogs):
    dfs = [parseTimingTable(path) for path in listOfLogs]
    df = pandas.concat(dfs).groupby('name').sum()
    #return a simple map
    vals = {
        name: cols["total_time"]
        for name, cols in df.to_dict('index').items()
    }
    return vals


def checkTimingVals(timeVals, grouping):
    """
    Returns the list of algorithms that are ran and not included in any grouping, and the list of algorithms that are expected but not found.
    """
    #Initialisation
    numberOfRegExpTriggered = {}
    for algName in timeVals:
        numberOfRegExpTriggered[algName] = 0
    numberOfTimesTriggered = {}
    for group, exprs in grouping.items():
        numberOfTimesTriggered[group] = {}
        for expr in exprs:
            numberOfTimesTriggered[group][expr] = 0
    #Scanning the timing values
    for algName in timeVals:
        for group, exprs in grouping.items():
            for expr in exprs:
                if re.match(expr, algName):
                    numberOfTimesTriggered[group][expr] += 1
                    numberOfRegExpTriggered[algName] += 1
    #Checks for algorithm names that match to no expression
    notInGrouping = []
    #Checks for algorithm names that match more than one expression (bad)
    triggersMore = []
    for algName in timeVals:
        if numberOfRegExpTriggered[algName] == 0:
            notInGrouping.append(algName)
        elif numberOfRegExpTriggered[algName] > 1:
            triggersMore.append(algName)
    #Checks for expressions that did not match anything
    notInTiming = []
    for group, exprs in numberOfTimesTriggered.items():
        for expr, val in exprs.items():
            if val == 0:
                notInTiming.append(expr)

    if len(notInGrouping) != 0:
        print(
            "WARNING: Following algorithms are in the timing table but are not associated with any group of algorithms"
        )
        for a in notInGrouping:
            print("\t", a)
    if len(notInTiming) != 0:
        print(
            "WARNING: Following algorithms are expected but are not found in the timing table"
        )
        for a in notInTiming:
            print("\t", a)
    if (len(triggersMore) != 0):
        print(
            "ERROR: Following expressions trigger more than one regular expression"
        )
        for a in triggersMore:
            print("\t", a)


def groupValues(groups, timeVals):
    """
    Gathers timing values in easy-to-understand groups. Also checks for algorithms that are not included in any grouping or some expected algorithms that are not present
    """
    # Checks which values are not in any grouping
    checkTimingVals(timeVals, groups)
    #TOTAL:
    groupedTimings = {}
    groupedTimings["Total"] = sum(timeVals.values())

    #Read them
    def find_group(k):
        for name, exprs in groups.items():
            if any(re.match(expr, k) for expr in exprs):
                return name
        return "Other"

    for name in groups:
        groupedTimings[name] = 0
    groupedTimings["Other"] = 0
    for k, v in timeVals.items():
        groupedTimings[find_group(k)] += v

    for k, v in groupedTimings.items():
        if k != "Total":
            groupedTimings[k] /= groupedTimings["Total"] / 100.
    return groupedTimings


def readTimings(groupName, listOfLogs):
    return groupValues(algGroups[groupName], parseAllTimingTables(listOfLogs))
