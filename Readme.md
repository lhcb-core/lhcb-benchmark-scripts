# LHCb benchmark scripts (MOVED TO [PrConfig](https://gitlab.cern.ch/lhcb-datapkg/PRConfig/))
This repository provides a set of scripts to benchmark Moore.
It is used to run [Performance and Regression](https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbPR) (PR) tests on top of nightly builds.

## Work locally to make flamegraphs or get throughputs
We recommend to work in the [lb-stack-setup](https://gitlab.cern.ch/rmatev/lb-stack-setup) environment.
Once the stack is set up and built, and `lhcb-benchmark-scripts` is cloned, making flamegraphs or throughputs becomes a one-liner.

Have a look at the current PR nightly tests for Moore, that live in `scripts`. These are shell scripts that just call the main `run_throughput_jobs.py` script with appropriate options. Most will run a throughput test first, and then a profiling test. You may want to (copy and) edit such a script. The script can then be executed in the runtime environment which has been previously set up like so:
```
../utils/build-env bash -c 'source /cvmfs/projects.cern.ch/intelsw/psxe/linux/19-all-setup.sh; ../Moore/build.$BINARY_TAG/run scripts/Moore_hlt1_reco_baseline.sh'
```
This assumes that you are in the `lhcb-benchmark-scripts` directory and your stack directory is one level above. Within the `lb-stack-setup` env, the Intel VTune tools have to be setup (`source /cvmfs/projects.cern.ch/intelsw/psxe/linux/19-all-setup.sh`). This is only needed if you want to run a profiling job (i.e. make a flamegraph). The rest of the command is calling the shell script in your custom built Moore environment.

### Input files
To get reliable throughput numbers, input files are copied to *local* (not mounted) storage. This copy is temporary and will be done every time a test is run (unless the exact same file was already downloaded). To speed up your local tests, we recommend to download the input manually and pass it to the test with the `-f` option.<br>
We ran into cornercases where input files couldn't be downloaded. If this happens, try to download it yourself to a local folder and pass this file to the `-f` option.<br>
In some cases the IO optimizations need to be switched off. Try setting `use_iosvc=False`, `event_store="HiveWhiteBoard"` in your Moore options if you see failures from `LHCb__MDF__IOSvcMM` 

### Cleaning up
Running local tests produces some output in the current directory. If you ran the commands from the `lhcb-benchmark-scripts` directory, you can clean up all these files with `./cleanup.sh` or `git clean`.<br> A dry run can be done with `git clean -nxd`. Running `git clean -fxd` will remove the files.

Running profiling jobs will also change the `palette.map` file. These changes should not be committed and you can ignore them with `git update-index --assume-unchanged palette.map`
