#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make flamegraph from vtune profiling output and "FlameBars" from timing table in log file.

Example:
    Assuming a vtune output directory in profile_out and a job log named Profile.log from a HLT1 job,
    you can run:
    make_profile_plots.py -r profile_out -l 'HLT1' --logs Profile.log
"""

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import subprocess
import os
import argparse

DIR = os.path.dirname(os.path.abspath(__file__))
STACK_COLLAPSE_SCRIPT = os.path.join(DIR, "stackcollapse-vtune.pl")
FLAME_GRAPH_SCRIPT = os.path.join(DIR, "flamegraph.pl")


def main(results_dir, listOfLogs, hltlabel, throughput):

    # First, make a flamegraph. The intel tools provide a report that can be read in by the perl scripts making the flamegraph later
    collect_profiling_results = [
        "amplxe-cl", "-R", "top-down", "-column", "CPU Time:Self,Module",
        "-report-out", "result.csv", "-format", "csv", "-csv-delimiter",
        "comma", "-r", results_dir
    ]
    try:
        subprocess.check_call(
            collect_profiling_results, stderr=subprocess.STDOUT)
    except:
        print(
            "Couldn't process vtune output directory. Please make sure the provided directory exists and amplxe-cl is in your PATH"
        )
        print("Arguments passed to the subprocess call were {}".format(
            collect_profiling_results))
        exit()

    subprocess.check_call(["sed", "-i", "1d", "result.csv"])

    # call the perl scripts from https://github.com/brendangregg/FlameGraph
    run_stackcollapse = [STACK_COLLAPSE_SCRIPT, "result.csv"]
    make_flamegraph = [
        FLAME_GRAPH_SCRIPT, "--cp", "--title", hltlabel + " Flame Graph",
        "--minwidth", "2", "--width", "1600"
    ]
    with open('flamy.svg', 'w') as output:
        sc_process = subprocess.Popen(
            run_stackcollapse, stdout=subprocess.PIPE)
        subprocess.check_call(
            make_flamegraph, stdin=sc_process.stdout, stdout=output)

    ## reads the text logs and extracts timing shares of different steps of the algorithm
    import readTimingTable
    timingTable = readTimingTable.readTimings(hltlabel, listOfLogs)

    # last entry in the sorted list will always be the total time so let's remove that one
    sortedList = [
        elem for elem in sorted(timingTable.items(), key=lambda item: item[1])
        if not (elem[0] == 'Total' or elem[1] < 0.1)
    ]

    # spread colors out over the hot_r colormap +1 makes sure last color isn't white
    colors = matplotlib.cm.hot_r(np.linspace(0., 1., len(sortedList) + 1))
    fig, ax = plt.subplots(figsize=(15, 10))

    pos = np.arange(len(sortedList)) + .5
    plt.barh(
        pos, [elem[1] for elem in sortedList], align='center', color=colors)
    plt.yticks(pos, [elem[0] for elem in sortedList], fontsize=18)
    plt.xticks(fontsize=18)

    #Add values to bars
    for i, elem in enumerate(sortedList):
        plt.text(
            elem[1],
            i + .5,
            '{0:.2f} '.format(elem[1]),
            va='center',
            color='black',
            fontweight='bold',
            fontsize=14)

    # make sure there is enough room to the right of the largest bar for the text
    plt.xlim(0, sortedList[-1][1] + 5)
    plt.xlabel(
        "Timing fraction within the " + hltlabel + " sequence [%]",
        fontsize=18)

    throughput_text = ''
    if throughput:
        if throughput >= 1e3:
            throughput = 1.e-3 * throughput
            tp_SI = 'k'
        else:
            tp_SI = ''

        throughput_text = r"{0} Throughput Rate {1:03.1f} {2}Hz".format(
            hltlabel, throughput, tp_SI)

    #Add LHCbSimulation & throughput text
    textStr = '\n'.join(("LHCb Simulation", '', throughput_text))
    ax.text(
        0.99,
        0.20,
        textStr,
        transform=ax.transAxes,
        fontsize=24,
        verticalalignment='top',
        horizontalalignment="right",
        bbox={
            'facecolor': 'white',
            'edgecolor': 'none'
        })

    plt.savefig("FlameBars.png")
    plt.savefig("FlameBars.pdf", bbox_inches='tight')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__.splitlines()[0])
    parser.add_argument(
        '-r',
        '--results',
        dest='results',
        type=str,
        default=None,
        required=True,
        help='vtune result directory')
    parser.add_argument(
        '--logs',
        dest='logs',
        type=str,
        default=None,
        required=True,
        nargs='+',
        help='List of log files')
    parser.add_argument(
        '-l',
        '--hltlabel',
        dest='hltlabel',
        type=str,
        required=True,
        choices=['HLT1', 'HLT2'],
        help=
        'Pick HLT configuration for plot labels and checking of algorithms in log file)'
    )
    parser.add_argument(
        '-t',
        '--throughput',
        dest='throughput',
        type=float,
        required=False,
        help='Events/s throughput number to be added as label to FlameBars')

    args = parser.parse_args()
    main(args.results, args.logs, args.hltlabel, args.throughput)
