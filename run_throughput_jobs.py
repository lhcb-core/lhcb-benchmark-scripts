#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Run one or more multithreaded jobs and measure throughput.

This is effectively a wrapper around gaudirun.py, which allows running
multi-job and multi-threaded jobs. It uses numactl if available to bind
each job to a numa domain. It also supports VTune profiling.

Examples:
    # HLT1 throughput
    Moore/run lhcb-benchmark-scripts/run_throughput_jobs.py -n 1e6 \
        '$HLT1CONFROOT/options/hlt1_pp_default.py'

    # HLT1 reconstruction throughput
    Moore/run lhcb-benchmark-scripts/run_throughput_jobs.py -n 1e6 \
        '$RECOCONFROOT/options/hlt1_reco_baseline.py'

    # HLT2 reconstruction throughput (like brunel)
    Moore/run lhcb-benchmark-scripts/run_throughput_jobs.py -n 1e4 \
        --test-file-db-key=UpgradeHLT1FilteredWithGEC \
        '$MOOREROOT/options/ft_decoding_v2.py' \
        '$RECOCONFROOT/options/hlt2_reco_brunelesque.py'

    # HLT2 reconstruction throughput
    Moore/run lhcb-benchmark-scripts/run_throughput_jobs.py -n 1e4 \
        --test-file-db-key=UpgradeHLT1FilteredWithGEC \
        '$MOOREROOT/options/ft_decoding_v2.py' \
        '$RECOCONFROOT/options/hlt2_reco_baseline.py'

    # HLT2 reconstruction throughput (many jobs)
    Moore/run lhcb-benchmark-scripts/run_throughput_jobs.py -n 250 -j 40 \
        --test-file-db-key=UpgradeHLT1FilteredWithGEC \
        '$MOOREROOT/options/ft_decoding_v2.py' \
        '$RECOCONFROOT/options/hlt2_reco_baseline.py'

    # HLT1 profile (vtune must be already set up)
    Moore/run lhcb-benchmark-scripts/run_throughput_jobs.py -n 1e5 \
        --profile \
        '$RECOCONFROOT/options/hlt1_reco_baseline.py'

    # HLT1 profile using lb-stack-setup
    utils/build-env bash -c '
        source /cvmfs/projects.cern.ch/intelsw/psxe/linux/all-setup.sh
        Moore/build.$BINARY_TAG/run \
            echo lhcb-benchmark-scripts/run_throughput_jobs.py \
                -n 1e5 --profile \
                \$RECOCONFROOT/options/hlt1_reco_baseline.py'

TODO:
- run each job (and profiler) in a different cwd
- integrate some of this into gaudirun.py?

"""
from __future__ import print_function, division
import atexit
import argparse
import logging
import math
import os
import re
import shutil
import socket
import subprocess
import tempfile
from itertools import cycle, islice

AVERAGE_EVENT_SIZE = 150 * 1000  # upper limit of average event size


def rep(x, length):
    return list(islice(cycle(x), length))


def lscpu():
    output = subprocess.check_output(['lscpu', '-p=NODE'])
    lines = [line for line in output.splitlines() if not line.startswith('#')]
    res = {
        'n_logical_cpus': len(lines),
        'n_numa_nodes': len(set(lines)),
    }
    assert res['n_logical_cpus'] % res['n_numa_nodes'] == 0
    return res


CPU_INFO = lscpu()
DEFAULT_CACHE_DIRS = {
    'default': [
        '/run/throughput/numa{}'.format(i)
        for i in range(CPU_INFO['n_numa_nodes'])
    ],
    # if special defaults are needed, add an item with FQDN as the key
}

# prefer XDG_RUNTIME_DIR which should be on tmpfs
FALLBACK_CACHE_DIR = os.getenv('XDG_RUNTIME_DIR', tempfile.gettempdir())


def default_cache_dirs():
    hostname = socket.getfqdn()
    dirs = DEFAULT_CACHE_DIRS.get(hostname, DEFAULT_CACHE_DIRS['default'])
    assert len(dirs) == CPU_INFO['n_numa_nodes']
    return dirs


def is_tmpfs(path):
    try:
        output = subprocess.check_output(['df', '-T', path])
    except subprocess.CalledProcessError:
        return False
    lines = output.splitlines()
    assert len(lines) == 2
    assert lines[0].split()[1] == 'Type'
    return lines[1].split()[1] == 'tmpfs'


def is_remote(url):
    return url.startswith('mdf:root:') or url.startswith('root:')


def has_command(cmd):
    try:
        with open(os.devnull, 'w') as FNULL:
            subprocess.check_call(cmd, stdout=FNULL, stderr=FNULL)
        return True
    except OSError as e:
        logging.debug(str(e))
        return False
    except subprocess.CalledProcessError as e:
        logging.debug(str(e))
        return e.returncode in [0, 1]


def run_gaudi_job(log_filename,
                  input_files,
                  args,
                  n_events,
                  numa_node=None,
                  profiler_numa_node=None,
                  parsed_throughput=0.):

    # build command line
    extra_options = """
from Moore import options
options.n_threads = {n_threads!r}
options.n_event_slots = {evt_slots!r}
options.evt_max = {n_evts!r}
options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
options.set_conds_from_testfiledb({TFDBkey!r})
options.input_type = 'MDF'
options.input_files = {inputs!r}
""".format(
        n_evts=n_events,
        evt_slots=args.evtSlots,
        n_threads=args.threads,
        inputs=input_files,
        TFDBkey=args.test_file_db_key)

    cmd = [
        'gaudirun.py', '--all-opts', '--output=options.py', '--option',
        extra_options
    ] + [os.path.expandvars(x) for x in args.options]

    # prepend the command line with numactl if needed
    if numa_node is not None:
        cmd = ["numactl", "-N",
               str(numa_node), "-m",
               str(numa_node), "--"] + cmd

    # run the test
    logging.debug("Launching job with cmd: {}".format(cmd))
    log_file = open(log_filename, 'w+')
    process = subprocess.Popen(cmd, stdout=log_file, stderr=subprocess.STDOUT)

    if args.profile:
        logging.info("Launching profiler")
        # Normally, one would wrap the executable with amplxe-cl:
        #     amplxe-cl [...] -- ./prog
        # However this leads to a significant drop in throughput for an unknown reason.
        # For now we work around this by delaying starting vtune such that it only
        # starts after initialize (the sleep) and detaches before
        # finalize (the -d flag).
        subprocess.Popen(["sleep", "60"]).wait()

        prof_cmd = [
            "amplxe-cl", "-collect hotspots", "-d", "60", "-target-pid=",
            str(process.pid), "-r", "profile_out"
        ]
        # At the end of collection the executable above does some preproscessing,
        # which should can affect the throughput measurement. Since we profile using
        # a single job bound to a single numa domain, we want to avoid affecting the
        # throughput such that it is easy to compare to a fully loaded machine.
        # For this reason, this script should be executed on a different numa domain.

        if profiler_numa_node is not None:
            prof_cmd = [
                "numactl", "-N",
                str(profiler_numa_node), "-m",
                str(profiler_numa_node), "--"
            ] + prof_cmd
        else:
            logging.warn("Profiler not bound to a (different) numa domain. "
                         "Throughput might be affected.")
        logging.debug("Launching profiling job with cmd: {}".format(prof_cmd))
        pro_log_file = open("vtune_output.log", 'w+')
        subprocess.Popen(
            prof_cmd, stdout=pro_log_file, stderr=subprocess.STDOUT)

        ## wait for the hijacked job to finish
        process.wait()
        logging.info("Making profile plots")
        for s in args.options:
            hlt_label = "HLT1" if "hlt1" in s.lower() else "HLT2"
        import make_profile_plots
        make_profile_plots.main("profile_out", [log_filename], hlt_label,
                                parsed_throughput)

    return process, log_file


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__.splitlines()[0])
    parser.add_argument('options', nargs='+', help='Gaudi options files.')
    parser.add_argument(
        '-j',
        '--jobs',
        dest='n_jobs',
        type=int,
        default=None,
        help='Number of processes to launch (defaults to # numa nodes)')
    parser.add_argument(
        '-t',
        '--threads',
        type=int,
        default=None,
        help='Number of threads per job (defaults to # logical cpus / # jobs.')
    parser.add_argument(
        '-e',
        '--evtSlots',
        type=int,
        help='Number of event slots per job (defaults to max(1.2 * # threads, '
        '1 + # threads)')
    parser.add_argument(
        '-n',
        '--events',
        default=1000,
        type=lambda x: int(round(float(x))),
        help='nb of events to process per job')
    parser.add_argument(
        '--outfileTag',
        default='',
        help="Output file name tag, name will be 'ThroughputTest_TAG_...'")
    parser.add_argument(
        '--test-file-db-key',
        default='MiniBrunel_2018_MinBias_FTv4_MDF',
        help='TestFileDB key defining input files and tags.')
    parser.add_argument(
        '-f',
        '--inputs',
        nargs='+',
        help='Names of input files, multiple names possible (defaults to '
        'files from TestFileDB entry if not given)')
    parser.add_argument(
        '--cache-dirs',
        default=None,
        help='Comma separated paths to directories, one per job, where the '
        'input files will be cached (default is hostname dependent or '
        '$XDG_RUNTIME_DIR).')
    parser.add_argument(
        '--nonuma',
        action='store_true',
        help='whether to disable usage of numa domains.')
    parser.add_argument(
        '--profile',
        action='store_true',
        help='Enable vtune profiling (only supported for single job).')
    parser.add_argument(
        '--debug', action='store_true', help='Debugging output')
    args = parser.parse_args()

    logging.basicConfig(
        format='%(levelname)-7s %(message)s',
        level=(logging.DEBUG if args.debug else logging.INFO))

    #the number of events could change for profiling jobs
    n_events = args.events
    throughput_for_plotlabel = 0

    if CPU_INFO['n_numa_nodes'] == 1:
        args.nonuma = True
    elif not args.nonuma:
        if not has_command(['numactl', '--help']):
            logging.warn('numactl not found, running without binding to nodes')
            args.nonuma = True

    if args.profile:
        if not has_command(['amplxe-cl', '--help']):
            raise RuntimeError(
                "amplxe-cl --help failed, please set up vtune with\n"
                "    source /cvmfs/projects.cern.ch/intelsw/psxe/linux/"
                "all-setup.sh")
        args.n_jobs = args.n_jobs or 1
        if args.n_jobs > 1:
            parser.error("VTune Profile only supported for single job")
        args.threads = args.threads or (
            CPU_INFO['n_logical_cpus'] // CPU_INFO['n_numa_nodes'])

        #search for latest throughput numbers in the default log file to potentially set the number of events, and label the FlameBars plot
        try:
            parsed_throughput = 0
            throughput_logfiles = [
                f for f in os.listdir(".") if 'ThroughputTest' in f
            ]
            for f in throughput_logfiles:
                lines = open(f, "r").readlines()
                for line in lines[::-1]:
                    m = re.search(r"Evts\/s = ([\d.]+)", line)
                    if m: parsed_throughput += float(m.group(1))
        except:
            logging.warn(
                "Cannot parse throughput numbers. Profiling will fail if you are running with -n=-1"
            )
            parsed_throughput = 0
        #parse the number of events such that the event loop runs for 3 minutes (assumung there were 2 throughput jobs)
        throughput_for_plotlabel = parsed_throughput
        if args.events == -1:
            n_events = int(parsed_throughput * 90.)
    else:
        args.n_jobs = args.n_jobs or (1 if args.nonuma else
                                      CPU_INFO['n_numa_nodes'])
        if args.n_jobs != CPU_INFO['n_numa_nodes']:
            logging.warn("There are {} available numa nodes but you are "
                         "launching {} jobs".format(CPU_INFO['n_numa_nodes'],
                                                    args.n_jobs))
        args.threads = args.threads or (
            CPU_INFO['n_logical_cpus'] // args.n_jobs)

    # check total number of threads
    if args.n_jobs * args.threads > CPU_INFO['n_logical_cpus']:
        logging.warn(
            "CPU has {} threads but you specified {} jobs with {} threads "
            "each. This will overcommit the CPU.".format(
                CPU_INFO['n_logical_cpus'], args.n_jobs, args.threads))

    if args.evtSlots is None:
        args.evtSlots = max(int(round(1.2 * args.threads)), 1 + args.threads)

    tag = ''
    if args.outfileTag != '':
        tag = '_' + args.outfileTag + '_'

    # Set up local directories where inputs are cached
    if args.cache_dirs:
        args.cache_dirs = args.cache_dirs.split(',')
    else:
        args.cache_dirs = default_cache_dirs()
        if any(not os.path.isdir(d) for d in args.cache_dirs):
            fallback_dir = tempfile.mkdtemp(
                prefix='throughput-', dir=FALLBACK_CACHE_DIR)
            logging.warn(
                'not all default cache dirs {!r} exist, using {}'.format(
                    args.cache_dirs, fallback_dir))
            args.cache_dirs = [fallback_dir] * args.n_jobs
            # if we use the fallback directory, clean up after ourselves
            atexit.register(shutil.rmtree, fallback_dir)
    if len(args.cache_dirs) < args.n_jobs:
        args.cache_dirs = rep(args.cache_dirs, args.n_jobs)
        logging.warn('Recycling cache directories to {!r}'.format(
            args.cache_dirs))

    if not args.inputs:
        from PRConfig.TestFileDB import test_file_db
        inputs_fns = test_file_db[args.test_file_db_key].filenames
    else:
        inputs_fns = args.inputs

    # Distribute input files between jobs
    # - give a different set of files to each job, if possible
    if len(inputs_fns) < args.n_jobs:
        logging.warn('Too few input files: inputs will be recycled. '
                     'Some events will be processed more than once.')
        # recycle the list of inputs and cut it off at the minimal length
        inputs_fns = (inputs_fns * args.n_jobs)[:args.n_jobs]
    # - distribute the inputs alternating between jobs
    job_inputs = [inputs_fns[i::args.n_jobs] for i in range(args.n_jobs)]
    # - download inputs, get job input size and recycle inputs if needed
    #   based on # events
    for i, inputs in enumerate(job_inputs):
        # download files
        if all(is_remote(url) for url in inputs):
            from Moore.qmtest.context import download_mdf_inputs_locally
            logging.info('Downloading inputs for job {} to {}'.format(
                i, args.cache_dirs[i]))
            inputs[:] = download_mdf_inputs_locally(
                inputs,
                args.cache_dirs[i],
                max_size=n_events * AVERAGE_EVENT_SIZE)
        elif any(is_remote(url) for url in inputs):
            parser.error('inputs must either be all xrootd or all local')
        # recycle if needed
        sizes = [os.path.getsize(fn) for fn in inputs]
        n_rep = int(math.ceil(n_events / (sum(sizes) / AVERAGE_EVENT_SIZE)))
        if n_rep > 1:
            logging.warn(
                'Inputs for job {} are likely too few, recycling by a factor '
                'of {}'.format(i, n_rep))
            inputs.extend(inputs * (n_rep - 1))
    # - check and warn if some input files are used more than once
    unique_inputs = set().union(*job_inputs)
    if len(unique_inputs) < sum(len(i) for i in job_inputs):
        logging.warn('Some input files are given more than once. '
                     'Some events might be processed more than once.')

    slow_inputs = [path for path in unique_inputs if not is_tmpfs(path)]
    if slow_inputs:
        logging.warn('Inputs {!r} not on tmpfs, reading might be slower'.
                     format(slow_inputs))

    # Start all jobs
    runningJobs = []
    logfile_prefix = "Profile" if args.profile else "ThroughputTest"
    for i_job in range(args.n_jobs):
        if not args.nonuma:
            numa_node = i_job % CPU_INFO['n_numa_nodes']
            # put profiler on a different numa node
            profiler_numa_node = i_job % CPU_INFO['n_numa_nodes'] + 1
        else:
            numa_node = None
            profiler_numa_node = None

        log_filename = ('{:s}{:s}.{:s}.{:d}t.{:d}j.{:d}e.{:d}.log'.format(
            logfile_prefix, tag, os.environ['BINARY_TAG'], args.threads,
            args.n_jobs, n_events, i_job))
        logging.info('Launching job {}...'.format(i_job))
        runningJobs.append(
            run_gaudi_job(log_filename, job_inputs[i_job], args, n_events,
                          numa_node, profiler_numa_node,
                          throughput_for_plotlabel))

    # Wait for jobs to finish and collect results
    n_failed_jobs = 0
    throughputs = []
    for i_job, (job, ofile) in enumerate(runningJobs):
        logging.info('Waiting for job {} to complete...'.format(i_job))
        retcode = job.wait()
        if retcode != 0:
            logging.warn(
                "non-zero return code from job {} with output file {}".format(
                    i_job, ofile.name))
            n_failed_jobs += 1
            if args.debug and n_failed_jobs == 1:
                ofile.seek(0)
                for line in ofile:
                    print(line, end='')
        if not args.profile:
            # set file index to top of file
            ofile.seek(0)
            # read all lines from file
            lines = ofile.readlines()
            for line in lines[::-1]:
                m = re.search(r"Evts\/s = ([\d.]+)", line)
                if m:
                    throughputs.append(float(m.group(1)))
                    print("Throughput of job {} is {} Evts/s.".format(
                        i_job, throughputs[-1]))
                    break
            ofile.close()

    if not args.profile:
        print(
            "Throughput test is finished. Overall reached Throughput is Evts/s = {}"
            .format(sum(throughputs)))
        if n_failed_jobs:
            exit(
                'Some job(s) failed: check the warnings above for the retcode')
